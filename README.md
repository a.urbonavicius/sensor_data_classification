# Sensor_Data_Classification

Sensor data classification - Gradient Boosting vs. Neural Network models

Inspired by the paper "Human activity recognition based on time series analysis using U-Net" by Yong Zhang, Yu Zhang et.al (https://arxiv.org/ftp/arxiv/papers/1809/1809.08113.pdf), decided to do my own version of this experiment to find out performance differences between high performer from Gradient Boosting camp aka XGBOOST vs. Neural Network models.

Dataset is from https://archive.ics.uci.edu/ml/datasets/human+activity+recognition+using+smartphones#

From Dataset description:
The experiments have been carried out with a group of 30 volunteers within an age bracket of 19-48 years. Each person performed six activities (WALKING, WALKING_UPSTAIRS, WALKING_DOWNSTAIRS, SITTING, STANDING, LAYING) wearing a smartphone (Samsung Galaxy S II) on the waist. Using its embedded accelerometer and gyroscope, we captured 3-axial linear acceleration and 3-axial angular velocity at a constant rate of 50Hz. The experiments have been video-recorded to label the data manually. The obtained dataset has been randomly partitioned into two sets, where 70% of the volunteers was selected for generating the training data and 30% the test data.